<?php

/**
 * Send an awkward image with optional type
 *
 * Usage: /awkward <type>
 */

require_once 'slack.php';

$slack = new Slack('zImiBACbkk4lUfFdPpaaKqDk');

$types = array('seal' => 'awkward-seal.gif', 'man' => 'awkward-man.jpg');

// If we don't have text make it random
if (!$slack->getText() || !array_key_exists(mb_strtolower($slack->getText()), $types)) {
    $file = $types[array_rand($types)];
} else {
    $file = $types[mb_strtolower($slack->getText())];
}

// Reply with the image
return $slack->sendReply("|<|https://slack.corednacdn.com/bots/assets/" . $file . "?c=z" . time() . "|*" . time() . "*|>|");

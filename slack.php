<?php

/**
 * Generic slack methods
 */

class Slack
{
    private $channelId;
    private $channelName;
    private $privateReplyUrl;
    private $publicReplyUrl;
    private $text;
    private $userId;
    private $username;

    /**
     * Create a new instance and validate token
     *
     * @param  string $token
     * @return void
     */
    public function __construct($token)
    {
        // Validate token
        if ($_POST['token'] != $token) {
            die('This command is not integrated correct, the token used to invoke this command is invalid');
        }

        // Capture settings
        $this->channelId = $_POST['channel_id'];
        $this->channelName = $_POST['channel_name'];
        $this->publicReplyUrl = $_POST['response_url'];
        $this->privateReplyUrl = $_POST['response_url'];
        $this->text = trim($_POST['text'], "*_~ \t\n\r\0\x0B");
        $this->userId = $_POST['user_id'];
        $this->username = ucfirst($_POST['user_name']);
    }

    /**
     * Get channel id from the original request
     *
     * @return string
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * Get channel name from the original request
     *
     * @return string
     */
    public function getChannelName()
    {
        return $this->channelName;
    }

    /**
     * Get text from the original request
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Get URL of this request
     *
     * @param  boolean $stripQueryString
     * @return string
     */
    public function getUrl($stripQueryString = false)
    {
        $scheme = 'http';
        if (array_key_exists('HTTP_X_FORWARDED_PROTO', $_SERVER)) {
            $scheme = trim(reset(explode(',', $_SERVER['HTTP_X_FORWARDED_PROTO'])));
        }
        $scheme = (array_key_exists('HTTPS', $_SERVER) || $scheme == 'https') ? 'https://' : 'http://';

        $hostname = $scheme . $_SERVER['HTTP_HOST'];
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $qs = htmlspecialchars(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), ENT_QUOTES, 'UTF-8');

        $uri = $stripQueryString || !$qs ? $path : $path . '?' . $qs;

        return $hostname . $uri;
    }

    /**
     * Get user id from the original request
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Get username from the original request
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Send a message back to the user
     *
     * @param  string $message
     * @return void
     */
    public function sendMessage($message)
    {
        return $this->sendReply($message, true);
    }

    /**
     * Send a reply back to slack
     *
     * @param  string  $message
     * @param  boolean $private
     * @param  string  $url
     * @return void
     */
    public function sendReply($message, $private = false)
    {
        // Set response type
        $responseType = ($private) ? 'ephemeral' : 'in_channel';
        $url = ($private) ? $this->privateReplyUrl : $this->publicReplyUrl;

        // Encode & < > and + in messages except for links
        $message = str_replace(array('&', '<', '>', '|&lt;|', '|&gt;|'), array('&amp;', '&lt;', '&gt;', '<', '>'), $message);

        // Send response to slack
        $reply = curl_init($url);
        curl_setopt($reply, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($reply, CURLOPT_POSTFIELDS, json_encode(array('response_type' => $responseType, 'text' => $message)));
        curl_setopt($reply, CURLOPT_RETURNTRANSFER, true);
        curl_exec($reply);
        curl_close($reply);
    }

    /**
     * Set the reply URL
     *
     * @param  string $url
     * @return void
     */
    public function setReplyUrl($url)
    {
        $this->publicReplyUrl = $url;
    }
}

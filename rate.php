<?php

/**
 * Rate a coffee based on your scientific evaluation
 *
 * /rate score [message]
 */

require_once 'slack.php';

$slack = new Slack('WiiZJ4v29L3QhsaZTZuYURTU');
$slack->setReplyUrl('https://hooks.slack.com/services/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');

// Extract rating and custom message
$text = explode(' ', $slack->getText(), 2);
$rating = array_shift($text);
$message = (count($text)) ? array_shift($text) : '';

// Rating could be either a number (69) or a rating (69/100) or an equation 2^8/50
$rating = (strpos($rating, '/') !== false) ? $rating : $rating . '/100';

// Sort out the actual rating
list($numerator, $denominator) = explode('/', $rating);

// Make sure we only have valid characters so we don't eval harmful scripts
if (!preg_match('/[0-9\+\-\*÷\^\(\)\.]+/', $numerator) || !preg_match('/[0-9\+\-\*÷\^\(\)\.]+/', $denominator)) {
    return $slack->sendMessage('Your rating *' . $rating . '* could not be understood, try something like */rate 50*, */rate 50/60* or */rate 50/51 great coffee!*');
}

// Process maths
$numerator = str_replace(array('^', '÷'), array(' ** ', '/'), $numerator);
$denominator = str_replace(array('^', '÷'), array(' ** ', '/'), $denominator);

eval("\$numerator = $numerator;");
eval("\$denominator = $denominator;");

// Can't have negative values
if ($numerator < 0 || $denominator <= 0) {
    return $slack->sendMessage('Your rating must be between 0 and 100. The rating *' . $rating . '* works out to be *' . round($numerator, 2) . '/' . round($denominator, 2) . '*');
}

// Work out real rating
if (($denominator > 0 && $denominator != 100) || $numerator . '/' . $denominator != $rating) {
    $devisedRating = round(($numerator / $denominator) * 100, 0);
}

// Determine stars
$stars = (isset($devisedRating)) ? $devisedRating : round($numerator);

if (!is_numeric($stars) || !$stars < 0 || $stars > 100) {
    if (isset($devisedRating)) {
        $error = 'Your rating must be between 0 and 100. The rating *' . $rating . '* works out to be *' . round($numerator, 2) . '/' . round($denominator, 2) . '* which is *' . $stars . '/100*';
    } else {
        $error = 'Your rating must be between 0 and 100, *' . $rating . '* is not valid. Try something like */rate 50*, */rate 50/60* or */rate 50/51 great coffee!*';
    }

    return $slack->sendMessage($error);
}

$reply = '*' . $slack->getUsername() . '* rated that coffee';
if (isset($devisedRating)) {
    $reply .= ' *' . $rating . '* which';

    if ($numerator . '/' . $denominator != $rating && $denominator != 100) {
        $reply .= ' works out to something like *' . round($numerator, 2) . '/' . round($denominator, 2) . '* or';
    } else {
        $reply .= ' ends up being';
    }

    $reply .= ' about *' . $devisedRating . '/100*';
} else {
    $reply .= ' *' . $rating . '*';
}

if ($message) {
    // Capture links
    preg_match_all("/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/i", $message, $links);

    // Remove from message
    $message = preg_replace("/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/i", '', $message);

    // Clean up whitespace
    $message = preg_replace('/\s+/', ' ', $message);
    if (trim($message)) {
        $reply .= ', saying "_' . $message . '_"';
    }
}

$reply .= "\n" . str_repeat(':star:', $stars) . "\n";
$reply .= getRating($stars);
$slack->sendReply($reply);

if (isset($links) && count($links)) {
    foreach ($links as $link) {
        if (count($link)) {
            $slack->sendReply('|<|' . $link[0] . '|>|');
        }
    }
}

// Notify user if they didn't write the message in #coffeesnobs
if ($slack->getChannelId() != 'G0P4S3LHX' && $slack->getChannelId() != 'G11CTD0MN') {
    $messages = array(
        'Chur cuz, we got your rating and have let #coffeesnobs know what you think :flag-nz:',
        'Thanks for the rating homie, the O.G.s in #coffeesnobs have been told :gun:',
    );

    $slack->sendMessage($messages[array_rand($messages)]);
}

return;

/**
 * Get a rating message based on the number of stars recieved
 *
 * @param  integer  $stars
 * @return string
 */
function getRating($stars)
{
    // Array is based on where the rating starts
    $ratings = array(
        0   => ":poop: You're shit :poop:",
        30  => ':poop: Despresso :thumbsdown:',
        50  => ":coffee: You're grounded! :confounded:",
        60  => ":coffee: There's worse, but not much... :thumbsdown:",
        70  => ':coffee: Seems like you gave it your best... shot :disappointed:',
        85  => ':coffee: Fine effort but no dice :no_good:',
        90  => ':coffee: I like this a-latte :thumbsup:',
        92  => ':coffee: Nice job, brew :flag-nz:',
        94  => ':coffee: Cafe quality :coffee:',
        96  => ':coffee: Super suxcessful coffee making :six:',
        98  => ":coffee: Damn son, you're a barista-r :star2:",
        99  => ':coffee: Words can not espresso how good this coffee is :coffee:',
        100 => ':coffee: Time to quit your job and become a hipster barista :raised_hands:',
    );

    $reply = '';

    foreach ($ratings as $rating => $message) {
        if ($rating <= $stars) {
            $reply = $message;
            continue;
        }

        // Rating is higher than stars so kill loop
        break;
    }

    return $reply;
}

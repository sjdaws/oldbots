<?php

/**
 * Notify #coffeesnobs you're about to make a coffee and invite others to join in
 *
 * Usage: /coffee [minutes] [message]
 */

require_once 'slack.php';

$slack = new Slack('BYzuqb2ZjSELifrJSR8N8Yne');
$slack->setReplyUrl('https://hooks.slack.com/services/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');

// Extract minutes and custom message
$text = explode(' ', $slack->getText(), 2);
$minutes = array_shift($text);
$message = (count($text)) ? array_shift($text) : '';

// Notify channel about the coffee
$reply = '*' . $slack->getUsername() . '* is going to make coffee';
if ($minutes) {
    $reply .= ' in about *' . $minutes . ' minute';
    $reply .= ($minutes == 1) ? '*' : 's*';
} else {
    $reply .= ' *now*';
}

if ($message) {
    $reply .= ', they said "_' . $message . '_"';
}

$reply .= "\n:coffee: Does anyone here want coffee? :coffee: :star: :star2:";

// Pass webhook to ensure it goes to the right channel no matter where it was invoked
$slack->sendReply($reply);

// Notify user if they didn't write the message in #coffeesnobs
if ($slack->getChannelId() != 'G0P4S3LHX' && $slack->getChannelId() != 'G11CTD0MN') {
    $messages = array(
        '#coffeesnobs has been told that you are making coffee :coffee:',
    );

    $slack->sendMessage($messages[array_rand($messages)]);
}

return;

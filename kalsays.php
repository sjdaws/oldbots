<?php

/**
 * Create a message from Kal
 *
 * Usage: /kalsays text
 */

require_once 'slack.php';

$slack = new Slack('BDRl1p4VHDaJvE2h2JH6PFtU');

if (!in_array(mb_strtolower($slack->getUsername()), array('chris', 'scott'))) {
    return $slack->sendMessage("I'm afraid I can't do that ~Dave~ " . $slack->getUsername());
}

if (!$slack->getText()) {
    return $slack->sendMessage('You need to specify some text to says');
}

return $slack->sendReply($slack->getText());

<?php

/**
 * Displays the kiddo finger guns
 *
 * Usage: /kiddo
 */

require_once 'slack.php';

$slack = new Slack('BLv40TybUMyPg9mSPH6WeaLW');

// Allow custom text
$message = $slack->getText() ?: "You can't use JavaScript for everything!";

// Reply with the image
return $slack->sendReply("|<|https://slack.corednacdn.com/bots/assets/kiddo.jpg?c=z" . time() . "|*" . $message . "*|>|");
